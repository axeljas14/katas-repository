package LombokB;

public class People {

    private static final String GREET = "hello";

    private final int age;
    private final String name;
    private final String lastName;
    private final String city;
    private final String job;

    public People(int age, String name, String lastName, String city, String job) {
        this.age = age;
        this.name = name;
        this.lastName = lastName;
        this.city = city;
        this.job = job;
    }

    public String greet(){
        return GREET +" my name is "+name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCity() {
        return city;
    }

    public String getJob() {
        return job;
    }

    public static String getGREET() {
        return GREET;
    }
}
