package MostFrequentItemCount;

public class MostFrequentItemCount {

    public static int mostFrequentItemCount(int[] collection) {
        int maximumValue = 0;
        for (int iterator1 = 0; iterator1 < collection.length; iterator1++){
            int count = 0;
            for (int iterator2 = 0; iterator2 < collection.length; iterator2++){
                if (collection[iterator1] == collection[iterator2]){    // [0] == [0] | [0] == [1] | [0] == [2] | [0] == [3]
                    count++;
                }
            }
            if (count > maximumValue){
                maximumValue = count;
            }
        }
        return maximumValue;
    }

}
