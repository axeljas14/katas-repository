package Canvas;

public class Canvas {

    private final int width;
    private final int height;
    private final String leftFormat;
    private int x2;
    private int y2;
    private int x1;
    private int y1;


    public Canvas(int width, int height) {
        this.height = height;
        this.width = width;
        this.leftFormat = "| %1s |%n";
        this.x2 = 0;
        this.y2 = 0;
        this.x1 = 0;
        this.y1 = 0;
    }

    public Canvas draw(int x1, int y1, int x2, int y2) {
        this.x2 = x2;
        this.y2 = y2;
        this.x1 = x1;
        this.y1 = y1;
        return this;
    }

    public Canvas fill(int x, int y, char ch) {
        return this;
    }

    private String createSpace(){
        String spaces = "";
        for (int iterator = 0; iterator < y2; iterator++){
            spaces += " ";
        }
        return spaces;
    }

    private String createFigures(){
        String figure = "";
        for (int iterator = 0; iterator < y2; iterator++){
            figure += "x";
        }
        return figure;
    }

    private void createLines(){
        String spaces = "";
        for (int iterator = -4; iterator < y2; iterator++){
            spaces += "-";
        }
        System.out.println(spaces);
    }


    public String drawCanvas() {
        String canvas = "";
        createLines();
        int result = 0;
        for (int iterator = 0; iterator < height; iterator++){
            if (iterator == x2){
                result = iterator;
                break;
            }
            else {
                System.out.format(leftFormat, createFigures());
            }
        }

        for (int iterator = 0; iterator < height - result; iterator++){
            System.out.format(leftFormat, createSpace());
        }

        createLines();

        return canvas;
    }
}
