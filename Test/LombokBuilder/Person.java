package LombokBuilder;

import lombok.Builder;

@Builder
public class Person {

    private static final String GREET = "hello";

    private final int age;
    private final String name;
    private final String lastName;
    private final String city;
    private final String job;


    public String greet(){
        return GREET +" my name is "+name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCity() {
        return city;
    }

    public String getJob() {
        return job;
    }

    public static String getGREET() {
        return GREET;
    }
}
