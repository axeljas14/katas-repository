package MisteryColors;

import java.util.ArrayList;
import java.util.List;

public class MysteryColorAnalyzerImpl implements MysteryColorAnalyzer{

    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        List<Color> colors = new ArrayList<>();

        for (Color mysteryColor : mysteryColors) {
            if (!colors.contains(mysteryColor)){
                colors.add(mysteryColor);
            }
        }

        return colors.size();
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int count = 0;

        for (Color mysteryColor: mysteryColors){
            if (mysteryColor.equals(color)){
                count += 1;
            }
        }

        return count;
    }
}
