package Test;

import LombokBuilder.Person;
import LombokB.People;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class PeopleTest {

    @Test
    public void testSampleWithBuilder() {
        Person person = Person.builder().name("Adam").lastName("Savage").age(25).city("San Francisco").job("Unchained Reaction").build();
        assertEquals(
                "Adam",
                person.getName()
        );
        assertEquals(
                "hello my name is Adam",
                person.greet()
        );
    }

    @Test
    public void testWithoutBuilder(){
        People people = new People(25, "Adam", "Savage", "San Francisco", "Unchained Reaction");
        assertEquals(
                "Adam",
                people.getName()
        );
        assertEquals(
                "hello my name is Adam",
                people.greet()
        );
    }
}
