package SumGroups;

import java.util.ArrayList;

public class SumGroups {

    public static int sumGroups(int[] arr) {
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> finalList = new ArrayList<>();
        int iterator2;

        for (int iterator1 = 0; iterator1 < arr.length; iterator1++) {
            int sum = arr[iterator1];
            for (iterator2 = iterator1 ; iterator2 < arr.length - 1; iterator2++) {
                if (arr[iterator1] % 2 == arr[iterator2 + 1] % 2) {
                    sum += arr[iterator2 + 1];
                }
                else {
                    break;
                }
            }
            list1.add(sum);
            iterator1 = iterator2;
        }

        for (int iterator1 = 0; iterator1 < list1.size(); iterator1++) {
            int sum2 = list1.get(iterator1);
            for (iterator2 = iterator1; iterator2 < list1.size() - 1; iterator2++) {
                if (list1.get(iterator1) % 2 == list1.get(iterator2 + 1) % 2) {
                    sum2 += list1.get(iterator2 + 1);
                }
                else {
                    break;
                }
            }
            iterator1 = iterator2;
            finalList.add(sum2);
        }
        return finalList.size();
    }

}
