package Test;

import MostFrequentItemCount.MostFrequentItemCount;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class MostFrequentCountTest {

    @Test
    public void tests() {
        assertEquals(2, MostFrequentItemCount.mostFrequentItemCount(new int[] {3, -1, -1}));
        assertEquals(5, MostFrequentItemCount.mostFrequentItemCount(new int[] {3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3}));
        assertEquals(0, MostFrequentItemCount.mostFrequentItemCount(new int[] {}));
        assertEquals(1, MostFrequentItemCount.mostFrequentItemCount(new int[] {0}));
    }

}
