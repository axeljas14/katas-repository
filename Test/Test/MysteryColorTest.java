package Test;

import MisteryColors.Color;
import MisteryColors.MysteryColorAnalyzerImpl;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.AssertJUnit.assertEquals;

public class MysteryColorTest {

    MysteryColorAnalyzerImpl mysteryColor = new MysteryColorAnalyzerImpl();

    @Test
    void testSomethingNumberOfDistinctColors(){
        ArrayList<Color> colors = new ArrayList<>();
        colors.add(Color.BLUE);
        colors.add(Color.GREEN);
        colors.add(Color.RED);
        colors.add(Color.RED);
        colors.add(Color.RED);
        colors.add(Color.GREEN);

        assertEquals(3,  mysteryColor.numberOfDistinctColors(colors));
    }

    @Test
    void testSomethingColorOccurrence(){
        ArrayList<Color> colors = new ArrayList<>();
        colors.add(Color.BLUE);
        colors.add(Color.GREEN);
        colors.add(Color.RED);
        colors.add(Color.RED);
        colors.add(Color.RED);
        colors.add(Color.GREEN);

        assertEquals(2,  mysteryColor.colorOccurrence(colors, Color.GREEN));
    }
}
